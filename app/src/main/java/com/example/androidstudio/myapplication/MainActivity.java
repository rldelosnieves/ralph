package com.example.androidstudio.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    String str1, str2, str3;
    int count, count2, count3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button enter = (Button) findViewById(R.id.button);


        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText txt = (EditText) findViewById(R.id.editText);
                str1 = txt.getText().toString();
                str2 = str1.replaceAll("[aeiouAEIOU]", "");
                TextView txtv = (TextView) findViewById(R.id.textView);
                txtv.setText(str2);
                count = txt.getText().length();
                count2 = txtv.getText().length();
                count3 = count - count2;
                EditText txt2 = (EditText) findViewById(R.id.editText2);
                txt2.setText(String.valueOf(count3));

            }

        });
    }
}